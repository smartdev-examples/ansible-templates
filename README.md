# Ansible Templates
    настройте переменные окружения 
    или укажите свои авторизационные данные в файле create_infrastructure.yml
    
    ANSIBLE_HWC_IDENTITY_ENDPOINT: "{{ sbaendpoint }}" 
    
    эти данные можно посмотреть в документации, скорее всего это
    https://iam.ru-moscow-1.hc.sbercloud.ru:443/v3
    
    ANSIBLE_HWC_USER: "пользователь" 
    ANSIBLE_HWC_PASSWORD: "пароль" 
    ANSIBLE_HWC_DOMAIN: "домен"

    запустите плейбук
    ansible-playbook create_infrastructure.yml
